#ifndef QUEUE_H
#define QUEUE_H
#include <iostream>
#define FIX_NUM 1


/* a queue contains positive integer values. */
typedef struct queue
{
	int* _queue;
	int _maxSize;
	int _count;
} queue;

//this func create new queue
void initQueue(queue* q, unsigned int size);
//this func clean and delete the queue
void cleanQueue(queue* q);

//this func add new element to the queue
void enqueue(queue* q, unsigned int newValue);
//this func telete the first element in the queue
int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif /* QUEUE_H */