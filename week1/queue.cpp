#include "queue.h"

void initQueue(queue* q, unsigned int size)
{
	q->_queue = new int[size];
	for (int i = 0; i < size; i++)
	{
		q->_queue[i] = 0;
	}
	q->_maxSize = size;
	q->_count = 0;

}
void cleanQueue(queue* q)
{
	delete[] &(q->_queue);
	q->_maxSize = 0;
	q->_count = 0;
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->_maxSize > q->_count)
	{
		q->_queue[q->_count] = newValue;
		q->_count++;
	}
}

int dequeue(queue* q)
{
	int toReturn = -1;
	if (q->_count > 0)
	{
		toReturn = q->_queue[0];
		for (int i = 0; i < (q->_maxSize) - FIX_NUM; i++)
		{
			q->_queue[i] = q->_queue[i + FIX_NUM];
		}
		q->_queue[(q->_maxSize) - FIX_NUM] = 0;
		q->_count--;
	}
	return(toReturn);
}