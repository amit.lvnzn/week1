#include "linkedList.h"

void addNode(stackNode** head, int num)
{
	if (*head)
	{
		stackNode* curr = *head;
		while (curr->next != nullptr)
		{
			curr = curr->next;
		}
		stackNode* newNode = new stackNode;
		newNode->number = num;
		newNode->next = nullptr;
		curr->next = newNode;
	}
}

void rmNode(stackNode** head)
{
	if (*head)
	{
		stackNode* curr = *head;
		while (curr->next->next != nullptr)
		{
			curr = curr->next;
		}
		delete(curr->next);
		curr->next = nullptr;
	}
}