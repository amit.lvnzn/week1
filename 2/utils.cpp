#include "utils.h"

void reverse(int* nums, unsigned int size)
{ 
	stack* s = new stack;
	initStack(s);
	for (int i = 0; i < size; i++)
	{
		push(s, nums[i]);
	}
	for (int i = 0; i < size; i++)
	{
		nums[i] = pop(s);
	}
	cleanStack(s);
	delete(s);
}
int* reverse10()
{
	int* nums = new int[SIZE];
	for (int i = 0; i < SIZE; i++)
	{
		std::cout << "enter number " << i + FIX_NUM << ": ";
		std::cin >> nums[i];
	}
	reverse(nums, SIZE);
	return(nums);
}