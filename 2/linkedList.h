#ifndef LINKEDLIST_H
#define LINKEDLIST_H

/* a positive-integer value stack, with no size limit */
typedef struct stackNode
{
	int number;
	struct stackNode* next;
} stackNode;

//this func add new node
void addNode(stackNode** head, int num);
//this func remove the last node
void rmNode(stackNode** head);

#endif // LINKEDLIST_H