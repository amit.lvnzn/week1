#ifndef UTILS_H
#define UTILS_H
#include "stack.h"
#include <iostream>
#define SIZE 10
#define FIX_NUM 1

//this func reverse a given array
void reverse(int* nums, unsigned int size);
//this func input 10 numbers and reverse them
int* reverse10();

#endif // UTILS_H