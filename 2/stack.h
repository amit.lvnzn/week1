#ifndef STACK_H
#define STACK_H
#include "linkedList.h"
#define FIX_NUM 1

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	int count;
	stackNode* head;
} stack;

//this func add new element to the stack
void push(stack* s, unsigned int element);
//this func get the number inside the stack and delete it
int pop(stack* s);

//this func create new stack
void initStack(stack* s);
//this func clean and delete the stack
void cleanStack(stack* s);

#endif // STACK_H