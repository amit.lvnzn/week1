#include "stack.h"

void push(stack* s, unsigned int element)
{
	addNode(&(s->head), element);
	s->count++;
}
int pop(stack* s)
{
	int toReturn = -1;
	stackNode* curr = s->head;
	if (s)
	{
		if (s->head)
		{
			for (int i = FIX_NUM; i < s->count; i++)
			{
				curr = curr->next;
			}
			toReturn = curr->number;
			rmNode(&(s->head));
			s->count--;
		}
	}
	return(toReturn);
}

void initStack(stack* s)
{
	s->head = new stackNode;
	s->head->number = 0;
	s->head->next = nullptr;
	s->count = FIX_NUM;
}
void cleanStack(stack* s)
{
	for (int i = s->count; i > FIX_NUM; i--)
	{
		rmNode(&(s->head));
	}
	delete(s->head);
	s->head = nullptr;
	s->count = 0;
}